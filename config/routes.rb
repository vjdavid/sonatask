Rails.application.routes.draw do
  get 'home/index'

  root 'home#index'

  #   resources :products

  resources :tasks, :users, except: [:new, :edit]

end
