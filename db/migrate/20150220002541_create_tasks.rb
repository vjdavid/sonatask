class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.boolean :finish
      t.date :expiration
      t.string :tags
      t.string :images

      t.timestamps
    end
  end
end
